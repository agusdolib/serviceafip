//Prueba de envio de mensaje SOAP para verificar el estado del WebService de AFIP.
var DOMParser = require('xmldom').DOMParser;
var token = 'D94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/Pgo8c3NvIHZlcnNpb249IjIuMCI+CiAgICA8aWQgc3JjPSJDTj13c2FhaG9tbywgTz1BRklQLCBDPUFSLCBTRVJJQUxOVU1CRVI9Q1VJVCAzMzY5MzQ1MDIzOSIgZHN0PSJDTj13c2ZlLCBPPUFGSVAsIEM9QVIiIHVuaXF1ZV9pZD0iMjY3MjI2OTA4NyIgZ2VuX3RpbWU9IjE1NzE4NjI4OTMiIGV4cF90aW1lPSIxNTcxOTA2MTUzIi8+CiAgICA8b3BlcmF0aW9uIHR5cGU9ImxvZ2luIiB2YWx1ZT0iZ3JhbnRlZCI+CiAgICAgICAgPGxvZ2luIGVudGl0eT0iMzM2OTM0NTAyMzkiIHNlcnZpY2U9IndzZmUiIHVpZD0iU0VSSUFMTlVNQkVSPUNVSVQgMjc0MzUyMzk0OTMsIENOPXBydWViYWRvbGliIiBhdXRobWV0aG9kPSJjbXMiIHJlZ21ldGhvZD0iMjIiPgogICAgICAgICAgICA8cmVsYXRpb25zPgogICAgICAgICAgICAgICAgPHJlbGF0aW9uIGtleT0iMjc0MzUyMzk0OTMiIHJlbHR5cGU9IjQiLz4KICAgICAgICAgICAgPC9yZWxhdGlvbnM+CiAgICAgICAgPC9sb2dpbj4KICAgIDwvb3BlcmF0aW9uPgo8L3Nzbz4K&lt';
var sign = 'HdY7lln90Rth4owxigOAnGr5B2PhUp5+xUtux1aRbZODrXguOPE1WjfISPsd8B3RSr9bNaIfHf4CDidxaN0NQDWEApXwJ62vGsu6LTaUsZcaKUNYF3KdpITHgR7l9ZflTvwiCz9RpAS6uz2VU5bWma1as2KjuQaOF/rX7/eqw8g=';
var response = soapRequest();
function soapRequest(){
    var str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ar="http://ar.gov.afip.dif.FEV1/">'+
         '<soapenv:Header/>'+
         '<soapenv:Body>'+
         '<ar:FECAESolicitar>'+
             '<ar:Auth>'+
			 '<ar:Token>D94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/Pgo8c3NvIHZlcnNpb249IjIuMCI+CiAgICA8aWQgc3JjPSJDTj13c2FhaG9tbywgTz1BRklQLCBDPUFSLCBTRVJJQUxOVU1CRVI9Q1VJVCAzMzY5MzQ1MDIzOSIgZHN0PSJDTj13c2ZlLCBPPUFGSVAsIEM9QVIiIHVuaXF1ZV9pZD0iMjY3MjI2OTA4NyIgZ2VuX3RpbWU9IjE1NzE4NjI4OTMiIGV4cF90aW1lPSIxNTcxOTA2MTUzIi8+CiAgICA8b3BlcmF0aW9uIHR5cGU9ImxvZ2luIiB2YWx1ZT0iZ3JhbnRlZCI+CiAgICAgICAgPGxvZ2luIGVudGl0eT0iMzM2OTM0NTAyMzkiIHNlcnZpY2U9IndzZmUiIHVpZD0iU0VSSUFMTlVNQkVSPUNVSVQgMjc0MzUyMzk0OTMsIENOPXBydWViYWRvbGliIiBhdXRobWV0aG9kPSJjbXMiIHJlZ21ldGhvZD0iMjIiPgogICAgICAgICAgICA8cmVsYXRpb25zPgogICAgICAgICAgICAgICAgPHJlbGF0aW9uIGtleT0iMjc0MzUyMzk0OTMiIHJlbHR5cGU9IjQiLz4KICAgICAgICAgICAgPC9yZWxhdGlvbnM+CiAgICAgICAgPC9sb2dpbj4KICAgIDwvb3BlcmF0aW9uPgo8L3Nzbz4K&lt</ar:Token>'+
			 '<ar:Sign>HdY7lln90Rth4owxigOAnGr5B2PhUp5+xUtux1aRbZODrXguOPE1WjfISPsd8B3RSr9bNaIfHf4CDidxaN0NQDWEApXwJ62vGsu6LTaUsZcaKUNYF3KdpITHgR7l9ZflTvwiCz9RpAS6uz2VU5bWma1as2KjuQaOF/rX7/eqw8g=</ar:Sign>'+
			 '<ar:Cuit>27435239493</ar:Cuit>'+
             '</ar:Auth>'+
                 '<ar:FeCAEReq>'+
                 '<ar:FeCabReq>'+
                     '<ar:CantReg>1</ar:CantReg>'+
                     '<ar:PtoVta>12</ar:PtoVta>'+
                     '<ar:CbteTipo>6</ar:CbteTipo>'+ 
                 '</ar:FeCabReq>'+
                '<ar:FeDetReq>'+
                     '<ar:FECAEDetRequest>'+
                             '<ar:Concepto>1</ar:Concepto>'+ 
                             '<ar:DocTipo>80</ar:DocTipo>'+ 
                             '<ar:DocNro>20111111112</ar:DocNro>'+
                             '<ar:CbteDesde>27</ar:CbteDesde>'+
                             '<ar:CbteHasta>27</ar:CbteHasta>'+
                             '<ar:CbteFch>20191023</ar:CbteFch>'+
                             '<ar:ImpTotal>0</ar:ImpTotal>'+ 
                             '<ar:ImpTotConc>0</ar:ImpTotConc>'+
                             '<ar:ImpNeto>0</ar:ImpNeto>'+
                             '<ar:ImpOpEx>0</ar:ImpOpEx>'+
                             '<ar:ImpTrib>0</ar:ImpTrib>'+
                             '<ar:ImpIVA>0</ar:ImpIVA>'+
                             '<ar:FchServDesde></ar:FchServDesde>'+
                             '<ar:FchServHasta></ar:FchServHasta>'+
                             '<ar:FchVtoPago></ar:FchVtoPago>'+
                             '<ar:MonId>PES</ar:MonId>'+
                             '<ar:MonCotiz>1</ar:MonCotiz>'+
                    '</ar:FECAEDetRequest>'+
                 '</ar:FeDetReq>'+
             '</ar:FeCAEReq>'+
         '</ar:FECAESolicitar>'+
      '</soapenv:Body>'+
    '</soapenv:Envelope>'
          function createCORSRequest(method, url) {
                      var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
                      var xhr = new XMLHttpRequest();
                      if ("withCredentials" in xhr) {
                          xhr.open(method, url, false);
                      } else if (typeof XDomainRequest != "undefined") {
                          alert
                          xhr = new XDomainRequest();
                          xhr.open(method, url);
                      } else {
                          console.log("CORS not supported");
                          alert("CORS not supported");
                          xhr = null;
                      }
                      return xhr;
                  }
          var xhr = createCORSRequest("POST", "https://wswhomo.afip.gov.ar/wsfev1/service.asmx?WSDL");
          if(!xhr){
           console.log("XHR issue");
           return;
          }
  
          xhr.onload = function (){
           var results = xhr.responseText;
           console.log(results);
           isOk(results);
        }
  
          xhr.setRequestHeader('Content-Type', 'text/xml');
          xhr.send(str);
   }

function isOk(results)
{
    var doc = new DOMParser().parseFromString(results, 'text/xml');
    var valueVouchNumber = doc.getElementsByTagName('CAE');
    var tempsVochNumber = valueVouchNumber[0].firstChild.nodeValue;
    var valueResult = doc.getElementsByTagName('Resultado');
    var tempsResult = valueResult[0].firstChild.nodeValue;
    var valueId = doc.getElementsByTagName('DocNro');
    var tempsId = valueId[0].firstChild.nodeValue;
    if(tempsResult.toString() === 'A' )
    {  
        console.log('La factura ha sido aprobada de manera correcta el numero de comprobante es:' + tempsVochNumber.toString());
        console.log('El dni del cliente es:' + tempsId.toString());
    }
}