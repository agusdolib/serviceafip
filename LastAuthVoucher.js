//Prueba de envio de mensaje SOAP para verificar el estado del WebService de AFIP.
var DOMParser = require('xmldom').DOMParser;
const fs = require('fs');
var lastVoucher;
module.exports = {
    SoapRequest: SoapRequest
}
function SoapRequest(){
        var i = 0;
        filePath = './files/access.txt';
        //Count lines
        fileBuffer =  fs.readFileSync(filePath);
        to_string = fileBuffer.toString();
        splitString = to_string.split("@");

        this.key = splitString[0];
        this.sign = splitString[1];
        var str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ar="http://ar.gov.afip.dif.FEV1/">'+
        '<soapenv:Header/>'+
        '<soapenv:Body>'+
           '<ar:FECompUltimoAutorizado>'+
           '<ar:Auth>'+
                '<ar:Token>'+this.key+'</ar:Token>'+
                '<ar:Sign>'+this.sign+'</ar:Sign>'+
                '<ar:Cuit>27435239493</ar:Cuit>'+
           '</ar:Auth>'+
              '<ar:PtoVta>12</ar:PtoVta>'+
              '<ar:CbteTipo>6</ar:CbteTipo>'+
           '</ar:FECompUltimoAutorizado>'+
        '</soapenv:Body>'+
     '</soapenv:Envelope>'
              function createCORSRequest(method, url) {
                          var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
                          var xhr = new XMLHttpRequest();
                          if ("withCredentials" in xhr) {
                              xhr.open(method, url, false);
                          } else if (typeof XDomainRequest != "undefined") {
                              alert
                              xhr = new XDomainRequest();
                              xhr.open(method, url);
                          } else {
                              console.log("CORS not supported");
                              alert("CORS not supported");
                              xhr = null;
                          }
                          return xhr;
                      }
              var xhr = createCORSRequest("POST", "https://wswhomo.afip.gov.ar/wsfev1/service.asmx?WSDL");
              if(!xhr){
               console.log("XHR issue");
               return;
              }
      
              xhr.onload = function (){
               var results = xhr.responseText;
               var doc = new DOMParser().parseFromString(results, 'text/xml');
               var valueVouchNumber = doc.getElementsByTagName('CbteNro');
               lastVoucher = valueVouchNumber[0].firstChild.nodeValue;
            }
      
              xhr.setRequestHeader('Content-Type', 'text/xml');
              xhr.send(str);
              console.log(lastVoucher);
              return lastVoucher;
}
