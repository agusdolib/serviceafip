//Prueba de envio de mensaje SOAP para verificar el estado del WebService de AFIP.
module.exports = {
    wsStatus : function(){
        var status;
        var str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ar="http://ar.gov.afip.dif.FEV1/"> '+
                    '<soapenv:Header/>'+
                    '<soapenv:Body>'+
                    '<ar:FEDummy/>'+
                    '</soapenv:Body>'+
                    '</soapenv:Envelope>'  
              function createCORSRequest(method, url) {
                          var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
                          var xhr = new XMLHttpRequest();
                          if ("withCredentials" in xhr) {
                              xhr.open(method, url, false);
                          } else if (typeof XDomainRequest != "undefined") {
                              alert
                              xhr = new XDomainRequest();
                              xhr.open(method, url);
                          } else {
                              console.log("CORS not supported");
                              alert("CORS not supported");
                              xhr = null;
                          }
                          return xhr;
                      }
              var xhr = createCORSRequest("POST", "https://wswhomo.afip.gov.ar/wsfev1/service.asmx?WSDL");
              if(!xhr){
               console.log("XHR issue");
               return;
              }
      
            xhr.onload = function (){
               var DOMParser = require('xmldom').DOMParser;

               var results = xhr.responseText;
               console.log(results);
               var doc = new DOMParser().parseFromString(results, 'text/xml');

               var valueXMLappServer = doc.getElementsByTagName('AppServer');
               var tempsAppServ = valueXMLappServer[0].firstChild.nodeValue;

               var valueXMLdbServer = doc.getElementsByTagName('DbServer');
               var tempsDbServ = valueXMLdbServer[0].firstChild.nodeValue;

               var valueXMLauthServer = doc.getElementsByTagName('AuthServer');
               var tempsAuthServ = valueXMLauthServer[0].firstChild.nodeValue;
               //La idea para mi seria devolver un 0 si esta funcionando bien el ws y un 0 si no funciona
               //para tomar decisiones a partir de esto.
               if(tempsAppServ.toString() === 'OK' && tempsDbServ.toString() === 'OK' && tempsAuthServ.toString() === 'OK'){  
                   status = 0;
               }
               else status = 1;
            }
      
              xhr.setRequestHeader('Content-Type', 'text/xml');
              xhr.send(str);
              return status;
    }
};


