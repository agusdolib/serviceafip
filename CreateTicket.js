//Generacion del ticketRequest para generar certificados.
const fs = require('fs');
const readline = require('readline');
var DOMParser = require('xmldom').DOMParser;

var text = '';
module.exports = { 
    ticket : function()
    {
        let date = new Date();
        var builder = require('xmlbuilder');
        var doc = builder.create('loginTicketRequest')
            .ele('header')
                .ele('uniqueId')
                .txt(Math.floor(date.getTime() / 1000)) 
                .up()
                .ele('generationTime')
                .txt(new Date(date.getTime() - 10800000).toISOString())
                .up()
                .ele('expirationTime')
                .txt(new Date(date.getTime() + 10200000).toISOString())
                .up()
            .up()
        .ele('service')
            .txt('wsfe')
        .end({ pretty: true });
        fs.writeFileSync('./files/MiLoginTicket.xml',doc,{encoding:'utf8',flag:'w'});
        this.getSignedCertificate();
        return this.readTA();

    },	

    //Funcion para ejecutar en la consola openssl con el certificado y la clave privada para obtener el TA
    getSignedCertificate : function()
    {
        const exec = require('child_process').exec;
        const child = exec('openssl smime -sign -in ./files/MiLoginTicket.xml -out ./files/MiLoginTicket.xml.cms -signer ./files/certificado.pem -inkey ./files/privada.key -nodetach -outform PEM',
            (error, stdout, stderr) => {
                if (error !== null) {
                    console.log(`exec error: ${error}`);
                }
        });

    },

    //Funcion para leer el certificado generado con openssl, despreciando la primera y ultima linea que son
    //el inicio y fin del certificado. La mayoria de pruebas que hice, el certificado tiene exactamente 
    //40 lineas, pero para evitar errores cada vez que se genere un nuevo certificado, cuento las lineas.
    readTA : function()
    {
        var i = 0;
        filePath = './files/MiLoginTicket.xml.cms';
        //Count lines
        fileBuffer =  fs.readFileSync(filePath);
        to_string = fileBuffer.toString();
        split_lines = to_string.split("\n");
        //readlines skipping the first and the last one.
        readline.createInterface({
            input: fs.createReadStream(filePath),
            terminal: false
        }).on('line', function(line) {
        if( i !== 0 && i < split_lines.length - 2 )
        {
                fs.appendFileSync('./files/MiLoginTicket.txt', line );
        }
        i++;

        });
        return this.soapRequest();
    },

    //Soap request, con el certificado ya generado, lo que retorne esta funcion voy a tener que analizarlo para
    //poder extraer el token y el sign
    soapRequest : function(){
        var doc = 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/Pgo8c3NvIHZlcnNpb249IjIuMCI+CiAgICA8aWQgc3JjPSJDTj13c2FhaG9tbywgTz1BRklQLCBDPUFSLCBTRVJJQUxOVU1CRVI9Q1VJVCAzMzY5MzQ1MDIzOSIgZHN0PSJDTj13c2ZlLCBPPUFGSVAsIEM9QVIiIHVuaXF1ZV9pZD0iMjkzNjEwODE2NCIgZ2VuX3RpbWU9IjE1NzIwMTc3MTIiIGV4cF90aW1lPSIxNTcyMDYwOTcyIi8+CiAgICA8b3BlcmF0aW9uIHR5cGU9ImxvZ2luIiB2YWx1ZT0iZ3JhbnRlZCI+CiAgICAgICAgPGxvZ2luIGVudGl0eT0iMzM2OTM0NTAyMzkiIHNlcnZpY2U9IndzZmUiIHVpZD0iU0VSSUFMTlVNQkVSPUNVSVQgMjc0MzUyMzk0OTMsIENOPXBydWViYWRvbGliIiBhdXRobWV0aG9kPSJjbXMiIHJlZ21ldGhvZD0iMjIiPgogICAgICAgICAgICA8cmVsYXRpb25zPgogICAgICAgICAgICAgICAgPHJlbGF0aW9uIGtleT0iMjc0MzUyMzk0OTMiIHJlbHR5cGU9IjQiLz4KICAgICAgICAgICAgPC9yZWxhdGlvbnM+CiAgICAgICAgPC9sb2dpbj4KICAgIDwvb3BlcmF0aW9uPgo8L3Nzbz4K' + '@' + 'Y+9gPPQBg5ApUChmSToZ6Bw0DKi4aU5oLFTSK6Rlh/fgFMOXW+mG3kLkhqYm4GRs/egEVa4L4hBTSbmgnoD6+wLnLqhhav8nqyc7IZaqfh+jUW0xEQhrMvQTdg9RSFxTR11o0xm8WrMQ+WBziqJvcrpE2wCz8vCP/KRbaObIM/U=';
        fs.writeFileSync('./files/access.txt',doc,{encoding:'utf8',flag:'w'});
    //     var loginTicket =  fs.readFileSync('./files/MiLoginTicket.txt');
    //     // const [cert, key];
    //     var str =   '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsaa="http://wsaa.view.sua.dvadac.desein.afip.gov">'+
    //     '<soapenv:Body>'+
    //     '<wsaa:loginCms>'+
    //         '<wsaa:in0>'+ loginTicket.toString() +'</wsaa:in0>'+
    //     '</wsaa:loginCms>'+
    //     '</soapenv:Body>'+
    // '</soapenv:Envelope>'
    //         function createCORSRequest(method, url) {
    //                     var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
    //                     var xhr = new XMLHttpRequest();
    //                     if ("withCredentials" in xhr) {
    //                         xhr.open(method, url, false);
    //                     } else if (typeof XDomainRequest != "undefined") {
    //                         alert
    //                         xhr = new XDomainRequest();
    //                         xhr.open(method, url);
    //                     } else {
    //                         console.log("CORS not supported");
    //                         alert("CORS not supported");
    //                         xhr = null;
    //                     }
    //                     return xhr;
    //                 }
    //         var xhr = createCORSRequest("POST", "https://wsaahomo.afip.gov.ar/ws/services/LoginCms?WSDL");
    //         if(!xhr){
    //         console.log("XHR issue");
    //         return;
    //         }
    //         fs.writeFileSync('./files/MiLoginTicket.txt','',{encoding:'utf8',flag:'w'});
    //         xhr.onload = function (){
    //         var results = xhr.responseText;
    //         var doc = new DOMParser().parseFromString(results, 'text/xml');
    //         var valueVouchNumber = doc.getElementsByTagName('token');
    //         var tempsVochNumber = valueVouchNumber[0].firstChild.nodeValue;

    //         console.log(tempsVochNumber);
    //         }
    //         xhr.setRequestHeader('Content-Type', 'text/xml');
    //         xhr.setRequestHeader('soapAction','https://wsaahomo.afip.gov.ar/ws/services/LoginCms');
    //         xhr.send(str);
    //         console.log(str);
    //         return {
    //             first: 'key',
    //             second: 'sign',
    //         };
    },
}